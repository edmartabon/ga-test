import mongoose from 'mongoose'

const schema = new mongoose.Schema({
  lead_id: { type: String },
  company: { type: String },
  email: { type: String },
  full_name: { type: String },
})


module.exports = mongoose.model('Lead', schema)
