import mongoose from 'mongoose'

const schema = new mongoose.Schema({
  access_token: { type: String },
  expire_at: {type: Date, default: Date.now, expires: 3600}
})

module.exports = mongoose.model('Oauth', schema)
