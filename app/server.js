import 'dotenv/config'
import express from 'express'
import bodyParser from 'body-parser'

import oauth from './routes/oauth'
import lead from './routes/lead'
import database from './services/database'
import errorHandler from './services/errorHandler'

const app = express()

database.connect()
app.use(bodyParser.json({ limit: '1mb' }))
app.use(bodyParser.urlencoded({ extended: true }))

app.use(oauth)
app.use(lead)
app.use(errorHandler)

app.listen(process.env.APP_PORT, process.env.APP_URL, () => {
  console.log(`Server listening to http://${process.env.APP_URL}:${process.env.APP_PORT}`)
})

module.exports = app
