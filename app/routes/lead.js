import express from 'express'
import controller from '../controllers/lead'

const router = express.Router()

router.get('/api/leads/sync', controller.sync)

router.get('/api/leads', controller.index)
router.post('/api/leads/', controller.store)
router.get('/api/leads/:id', controller.show)
router.put('/api/leads/:id', controller.update)
router.delete('/api/leads/:id', controller.delete)


module.exports = router
