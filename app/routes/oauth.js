import express from 'express'
import controller from '../controllers/oauth'

const router = express.Router()

router.get('/oauth', controller.oauth)
router.get('/oauth2callback', controller.callback)

module.exports = router
