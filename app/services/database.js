import mongoose from 'mongoose'

mongoose.Promise = global.Promise

const database = {

  options: {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
  },

  async connect() {
    try {
      const dbConfig = process.env.DB_HOST
      await mongoose
        .connect(dbConfig, database.options)

      console.log('Database connected.')
    } catch (err) {
      console.log(err)
    }
  },

  disconnect() {
    mongoose.disconnect()
  },
}

module.exports = database
