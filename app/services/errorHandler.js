import boom from 'boom'

const errorHandler = (err, req, res, next) => {
  
  const error = {
    code: 500,
    message: 'Internal error',
    fields: []
  }

  if (boom.isBoom(err)) {
    const errOutput = err.output
    let message = errOutput.payload.message
    error.code = errOutput.statusCode

    if (err.data) {
      message = err.data
    }
    error.message = { message }
  }
  res.status(error.code).send(error.message)
}

module.exports = errorHandler