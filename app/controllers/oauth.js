import Boom from 'boom'
import axios from 'axios'
import qs from 'qs'

import Oauth from '../models/oauth'

module.exports = {
  
  oauth(req, res, next) {
    res.redirect(`https://accounts.zoho.com/oauth/v2/auth?client_id=${process.env.ZOHO_CLIENT_ID}&scope=ZohoCRM.modules.ALL,ZohoCRM.org.all,ZohoCRM.org.all&response_type=code&access_type=online&redirect_uri=http://localhost:3000/oauth2callback`)
  },

  async callback(req, res, next) {
    const accountServer = req.query['accounts-server']
    const code = req.query.code
    
    try {
      const token = await axios.post(`${accountServer}/oauth/v2/token`, qs.stringify({
        grant_type: 'authorization_code',
        client_id: process.env.ZOHO_CLIENT_ID,
        client_secret: process.env.ZOHO_SECRET_ID, 
        redirect_uri: 'http://localhost:3000/oauth2callback',
        code
      }, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
      }))
      
      const { access_token } = token.data
      await Oauth.findOneAndUpdate(
        { access_token },
        { access_token },
        { upsert: true, new: true }
      )
      res.send('success')
        
    } catch(err) {
      next(Boom.badImplementation(err))
    }
  }

}