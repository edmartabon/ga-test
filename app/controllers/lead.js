import Boom from 'boom'
import Lead from '../models/lead'
import Axios from 'axios'

import Oauth from '../models/oauth'

const controller = {

  async index(req, res, next) {
    try {
      const leads = await Lead.find()
      res.send(leads)
    } catch (err) {
      next(Boom.badImplementation(err))
    }
  },

  async store(req, res, next) {
    const data = req.body
    try {
      const lead = await Lead.findOneAndUpdate(
        { name: data.name },
        { name: data.name },
        { upsert: true, new: true }
      )
      res.send(lead)
    } catch (err) {
      next(Boom.badData(err))
    }
  },

  async show(req, res, next) {
    try {
      const lead = await Lead.findOne({
        _id: req.params.id
      })

      if (!lead) {
          return res.status(404).send({ message: 'lead not found' })
      }
      res.send(lead)
    }
    catch (err) {
      next(Boom.badImplementation(err))
    }
  },

  async update(req, res, next) {
    const data = req.body
    try {
      const lead = await Lead.findOneAndUpdate(
        { _id: req.params.id },
        data,
        { new: true }
      )

      if (!lead) {
        return res.status(404).send({ message: 'Lead not found' })
      }
      res.send(lead)
    }
    catch (err) {
      next(Boom.badData(err))
    }
  },

  async delete(req, res, next) {
    try {
      const lead = await Lead.findByIdAndRemove(req.params.id)
      res.send(lead)
    }
    catch (err) {
      next(Boom.badImplementation(err))
    }
  },

  async sync(req, res, next) {
    try {
      const oauth = await Oauth.findOne()
      if (oauth == null) {
        res.send(`
          No token found. <a href="/oauth">Click Here authenticate</a>
        `)
        return
      }

      const data = await Axios.get('https://www.zohoapis.com/crm/v2/leads', {
        headers: { 'Authorization': `Zoho-oauthtoken ${oauth.access_token}` }
      })

      const leads = data.data
      const newLeads = []
      
      for(let i of leads.data) {
        newLeads.push(Lead.findOneAndUpdate(
          { lead_id: i.id },
          {
            lead_id: i.id,
            company: i.Company,
            email: i.Email,
            full_name: i.Full_Name
          },
          { upsert: true, new: true }
        ))
      }
      
      const result = await Promise.all(newLeads)
      res.send(result)
    }
    catch (err) {
      next(Boom.badImplementation(err))
    }
  }
}

module.exports = controller
