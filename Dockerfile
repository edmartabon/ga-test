FROM node:8.9.1

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

RUN npm install

run rm -f .npmrc

COPY . /usr/src/app

RUN npm run test

EXPOSE 4400

CMD ["npm", "run", "start"]
